// Add new row

const table = document.getElementById("studentsTable");
const tableBody = document.getElementById("tableBody");
const modal = document.getElementById("form_block");
const form = document.querySelector('.form');
const submit_btn = document.getElementById('submitData');
const delete_confirm = document.getElementById("delete_ok");
const cancels = document.querySelectorAll('.close_warning');
const close_form = document.querySelector('.close_form');
const delete_block = document.querySelector('.delete_block');
const messagebtn = document.getElementById("notificationBtn");
const usernametext = document.querySelector('.Username_deletion');
const messagemenu = document.querySelector('.drop_down_message_menu');

document.addEventListener('DOMContentLoaded', function() {
    if (window.innerWidth <= 800) {

        messagebtn.addEventListener('click', () => {
            if(messagemenu.style.display == "block"){
                messagemenu.style.display = "none";
            }else{
                menu.style.display = "none";
                messagemenu.style.display = "block";
            }
            
        });
    }
});

cancels.forEach(cancel => {
    cancel.addEventListener('click', () => {
        delete_block.close()
    });
});

close_form.addEventListener('click', () => {
    modal.close()
});

function updateFormForEditingMode(id) {
    const formHeader = document.getElementById("dialog_header_text");
    const submitButton = document.getElementById("submitData");
    
    if(id){
        formHeader.textContent = 'Edit student';
        submitButton.textContent = 'Save';
    }else{
        formHeader.textContent = 'Add student';
        submitButton.textContent = 'Add';
    }
}

function assignBufferValuesFromRow(row){
    const children = row ? row.children : [];
    bufferObj.group = children[1].getAttribute('group');
    bufferObj.firstName = children[2].getAttribute('frstname');
    bufferObj.lastName = children[2].getAttribute('lastname');
    bufferObj.gender = children[3].getAttribute('gender');
    bufferObj.birthday = children[4].getAttribute('b_date');
    const statusDot = row.querySelector('.check_mark');
    bufferObj.status = statusDot.getAttribute('check_m');
}

function assignBufferValuesFromForm(){
    bufferObj.group = document.getElementById("group_choise").value;
    bufferObj.firstName = document.getElementById("first_name").value;
    bufferObj.lastName = document.getElementById("last_name").value;
    bufferObj.gender = document.getElementById("gender").value;
    let birthday = document.getElementById("birthday_date").value;
    let parts = birthday.split('-');
    bufferObj.birthday = parts[2] + '.' + parts[1] + '.' + parts[0];
    bufferObj.status = (form.querySelector('.switch_checkbox').checked) ? 1 : 0;
}

function fillFormWithBufferValues(){
    document.getElementById("id").value = bufferObj.id;
    document.getElementById("group_choise").value = bufferObj.group;
    document.getElementById("first_name").value = bufferObj.firstName;
    document.getElementById("last_name").value = bufferObj.lastName;
    document.getElementById("gender").value = bufferObj.gender;
    let particles = bufferObj.birthday.split('.');
    document.getElementById("birthday_date").value = particles[2] + '-' + particles[1] + '-' + particles[0];
    const stTxt = document.querySelector('.status');
    document.querySelector('.switch_checkbox').checked = (bufferObj.status != 0) ? true : false;
    stTxt.textContent = (bufferObj.status != 0 ) ? 'Active' : 'Inctive';
}

function validateForm() {
    const group = document.getElementById("group_choise").value;
    const firstName = document.getElementById("first_name").value;
    const lastName = document.getElementById("last_name").value;
    const gender = document.getElementById("gender").value;
    const birthday = document.getElementById("birthday_date").value;
    const error_msg = document.getElementById("valid_error");

    error_msg.classList.add('hidden');
    if (!group || !firstName || !lastName || !gender || !birthday) {
        error_msg.classList.remove('hidden');
        return false; 
    }
  
    return true;
}

function addRow() {
    const newRow = document.createElement('tr');
  
    const data_ID = (bufferObj.id || `${table.rows.length}`);
    const selectElement = document.getElementById("group_choise");
    const group = selectElement.querySelector(`option[group_id="${bufferObj.group}"]`);
    const groupTxt = group ? group.textContent: '';

    const selectGender = document.getElementById("gender");
    const gender = selectGender.querySelector(`option[gd_id="${bufferObj.gender}"]`);
    const genderTxt = gender ? gender.textContent: '';

    var statusClass = (bufferObj.status === 1) ? 'active' : 'non-active'; 
    console.log( bufferObj.group);
    newRow.setAttribute('Row-data_id', data_ID);
    newRow.innerHTML = `
    <th scope="row" class="check_sect">
      <input type="checkbox" class="students-checkbox">
    </th>
    <th class="group" group="${bufferObj.group}" scope="row">${groupTxt}</th>
    <td class="name" frstname="${bufferObj.firstName}" lastname="${bufferObj.lastName}">${bufferObj.firstName} ${bufferObj.lastName}</td>
    <td class="gender" gender="${bufferObj.gender}">${genderTxt}</td>
    <td class="b_date" b_date="${bufferObj.birthday}">${bufferObj.birthday}</td>
    <td><div class="check_mark ${statusClass}" check_m="${bufferObj.status}"></div></td>
    <td class="table_btn_sect">
      <button data_id="${data_ID}"class="action table_btn">
          <i data_id="${data_ID}" class="fa-solid fa-pencil action edit_img"></i>
      </button>
      <button data_id="${data_ID}" class="delete_btn table_btn">
          <i data_id="${data_ID}" class="fa-solid fa-xmark delete_img"></i>
      </button>
    </td>
  `;


    return newRow;
}

function deleteRow(row) {
    const table = document.getElementById("tableBody");
    const cell = row.getElementsByTagName('td');
    
    usernametext.textContent = "Are you sure you want to delete user " + cell[0].innerText + "?" ;
    delete_block.showModal();
    
    delete_confirm.addEventListener('click', () => {
        if (row) {
            console.log(table);
            table.removeChild(row);
        }
        delete_block.close();
    });
    
}

document.addEventListener("DOMContentLoaded", function() {

    document.addEventListener('click', function(event) {
      var target = event.target;

      if (target.hasAttribute('data_id')) {
        if(target.classList.contains('action')){
            let dataId = target.getAttribute('data_id');
            updateFormForEditingMode(dataId);

            bufferObj = {
                id: "",
                group: "",
                firstName: "",
                lastName: "",
                gender: "",
                birthday: "",
                status: ""
            };

            if(dataId){
                bufferObj.id = dataId;
                const tableRow = event.target.closest('tr');
                assignBufferValuesFromRow(tableRow);
            }
            
            fillFormWithBufferValues();
            modal.showModal();
            
        }else{
            deleteRow(target.closest('tr'));
        }
      }
    });

    submit_btn.addEventListener('click', function(event) {
        event.preventDefault(); 

        if (!validateForm()) {
            return;
        }

        assignBufferValuesFromForm();
        const newRow = addRow();
        tableBody.appendChild(newRow);

        console.table(bufferObj.id)
        if (bufferObj.id) {
            const row = document.querySelector(`[Row-data_id="${bufferObj.id}"]`);
            if (row) {
                row.parentNode.replaceChild(newRow, row);
            }
        }

        if (Object.keys(bufferObj).length !== 0) {
            const jsonString = JSON.stringify(bufferObj, null, 2); 
        } 

        modal.close();
    });

    const slider = document.querySelector('.slider');
    const statusText = document.querySelector('.status');

    slider.addEventListener('click', function() {
        
        if(!this.checked){
            this.checked = true;
            statusText.textContent = 'Active';
            slider.classList.add('checked'); 
        }else if(this.checked){
            this.checked = false;
            statusText.textContent = 'Inactive';
            slider.classList.remove('checked'); 
        }
    });
});
